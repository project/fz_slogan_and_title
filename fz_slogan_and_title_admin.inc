<?php

/**
 * implements hook_help()
 */
function fz_slogan_and_title_help($path, $arg) {
  $output = NULL;
  switch ($path) {
    case 'admin/help#fz_slogan_and_title':
      $output = '<h3>' .t('About'). '</h3>';
      $path   = drupal_get_path('module', 'fz_slogan_and_title') . '/fz_slogan_and_title.info';
      $info   = drupal_parse_info_file($path);
      $output .= 'Author: <a href="http://www.fzolee.hu">Zoltan Fabian</a><br/>';
      $output .= t('version').': '.$info['version'].'<br/>';
      $output .= t('compiled').': '.date('Y.m.d',$info['datestamp']);
      $output .= '<p>' . t('Drupal\'s default page change random or cyclic') . '</p>';
      $output .= '<p>' . t('FZ Slogan and Title provides random or cyclic contorol over the &lt;title> and slogan  (&lt;h1> )element on the page');
      $output .= '<p>' . l(t('More Help...'), 'admin/help/config/search/fz_slogan_and_title') . '</p>';
      break;
  }
  return $output;
}

/**
 * Implements hook_permission().
 */
function fz_slogan_title_permission() {
  return array(
      'administer fz_slogan_and_title' => array(
          'title' => t('Administer fz_slogan_and_title'),
          'restrict access' => TRUE,
          'description' => t('Administer FZ Slogan and Title.'),
      ),
  );
}

/**
 * Implements hook_menu().
 */
function fz_slogan_title_menu() {
  $items = array();

  $items['admin/config/fz/fz_slogan_and_title'] = array(
      'title'            => 'FZ Slogan and Title',
      'description'      => 'Adjust FZ Slogan and Title settings.',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('fz_slogan_and_title_settings'),
      'access arguments' => array('administer site configuration'),
      'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 *  Admin interface for FZ Slogan and Title
 */
function fz_slogan_and_title_settings(){
  $form = array();

 /* $all = variable_get('fz_slogan_title_all',True);
  $form['fz_slogan_title_all'] = array(
      '#type'  => 'checkbox',
      '#title' => t('Slogan and Title the same in every page'),
      '#default_value' => $all,
      '#description' => t("The titles and slogans are the same all the time"),
  );
  */
  $form['fz_slogan_and_title_scycle'] = array(
      '#type'  => 'radios',
      '#title' => t('Choose between random or cyclic using in sloganes'),
      '#default_value' => variable_get('fz_slogan_and_title_scycle', 'random'),
      '#options' => array('random' => t('Random'), 'cycling' => t('Cycling')),
      '#description' => t("Every rown new sentence"),
  );
  
  $slogans= variable_get("fz_slogan_and_title_slist");

  $form['fz_slogan_and_title_slist'] = array(
      '#type' => 'textarea',
      '#title' => t('Sentences for slogans'),
      '#default_value' => $slogans,
      '#description' => t("Every rown new sentence")
  );
  
  $sidx = variable_get('fz_slogan_and_title_sindex',0);
  $texts = preg_split("/\n/",$slogans );

  $form['fz_slogan_title_sactual'] = array(
      '#type'          => 'textfield',
      '#title'         => t('Actual sentence'),
      '#default_value' => $texts[$sidx],
      '#description'   => t("Actual slogan"),
      '#attributes'    => array('readonly' => 'readonly'),
  );
  $form = system_settings_form($form);
  return $form;
}
